import maya.api.OpenMaya as om
import maya.api.OpenMayaAnim as oma
import maya.cmds as mc

from nod.decorators.property import DelayedProperty
from nod.channels.base import NodChannel

from nod.nodetypes.deformer import NodDeformer


class NodBlendShape(NodDeformer):

    DEFORMER_TYPE = 'kBlendShape'

    def __init__(self, *args):
        super(NodBlendShape, self).__init__(*args)

    @DelayedProperty
    def fn(self):
        """
        Get BlendShape MFn function set for the current node

        Returns:
            maya.api.OpenMaya.MFnGeometryFilter
        """
        return oma.MFnGeometryFilter(self.dependNode)

    @property
    def weights(self):
        """
        Get weight channels

        Returns:
            list: List of nod.channels.base.NodChannel
        """
        return [NodChannel(self, 'weight[{}]'.format(i)) for i in xrange(mc.blendShape(self.name(), wc=True, q=True))]

    @DelayedProperty
    def weightAliasNames(self):
        """
        Get weights alias names as list of strings

        Returns:
            list: List of weight attribute alias names
        """
        return [a[0] for a in self.fn.getAliasList()]

    def nextAvailableTargetIndex(self):
        """
        Next available target index

        Returns:
            int: Index
        """
        return len(self.weights)

    def addTarget(self, target, weight=1):
        """
        Adds a given mesh target to the current blendShape node

        Args:
            target (str or nod.nodetypes.dag.NodDag: Target geometry to add to the blendShape node
            weight (float): Target weight value
            topologyCheck (bool): If True validate topology compatibility
        """
        geometry = self.outputGeometry()[0]
        mc.blendShape(self.name(), e=True, t=(str(geometry), self.nextAvailableTargetIndex(), str(target), weight))
