import maya.cmds as mc

from nod.decorators.property import DelayedProperty
from nod.nodetypes.transform import NodTransform
from nod.channels.base import NodChannel

class _OffsetMixin(object):

    def __init__(self, *args):
        pass

    @DelayedProperty
    def offset(self):
        return NodChannel(self, 'offset')

    @DelayedProperty
    def offsetX(self):
        return NodChannel(self, 'offsetX')

    @DelayedProperty
    def offsetY(self):
        return NodChannel(self, 'offsetY')

    @DelayedProperty
    def offsetZ(self):
        return NodChannel(self, 'offsetZ')


class _ConstraintTranslateMixin(object):

    def __init__(self, *args):
        pass

    @DelayedProperty
    def constraintTranslate(self):
        return NodChannel(self, 'constraintTranslate')

    @DelayedProperty
    def constraintTranslateX(self):
        return NodChannel(self, 'constraintTranslateX')

    @DelayedProperty
    def constraintTranslateY(self):
        return NodChannel(self, 'constraintTranslateY')

    @DelayedProperty
    def constraintTranslateZ(self):
        return NodChannel(self, 'constraintTranslateZ')


class _ConstraintRotateMixin(object):

    def __init__(self, *args):
        pass

    @DelayedProperty
    def constraintRotate(self):
        return NodChannel(self, 'constraintRotate')

    @DelayedProperty
    def constraintRotateX(self):
        return NodChannel(self, 'constraintRotateX')

    @DelayedProperty
    def constraintRotateY(self):
        return NodChannel(self, 'constraintRotateY')

    @DelayedProperty
    def constraintRotateZ(self):
        return NodChannel(self, 'constraintRotateZ')


class _ConstraintScaleMixin(object):

    def __init__(self, *args):
        pass

    @DelayedProperty
    def constraintScale(self):
        return NodChannel(self, 'constraintScale')

    @DelayedProperty
    def constraintScaleX(self):
        return NodChannel(self, 'constraintScaleX')

    @DelayedProperty
    def constraintScaleY(self):
        return NodChannel(self, 'constraintScaleY')

    @DelayedProperty
    def constraintScaleZ(self):
        return NodChannel(self, 'constraintScaleZ')


class _ConstraintVectorMixin(object):

    def __init__(self, *args):
        pass

    @DelayedProperty
    def constraintVector(self):
        return NodChannel(self, 'constraintVector')

    @DelayedProperty
    def constraintVectorX(self):
        return NodChannel(self, 'constraintVectorX')

    @DelayedProperty
    def constraintVectorY(self):
        return NodChannel(self, 'constraintVectorY')

    @DelayedProperty
    def constraintVectorZ(self):
        return NodChannel(self, 'constraintVectorZ')


class NodConstraint(NodTransform):

    TYPE_NAME = None

    def __init__(self, *args):
        super(NodConstraint, self).__init__(*args)

    @property
    def mayaCommand(self):
        """
        Returns maya command function e.g. mc.parentConstraint
        Returns:
            func
        """
        return getattr(mc, self.TYPE_NAME)

    def targets(self):
        """
        Returns the names of the attributes that control the weight of the target objects.
        Aliases are returned in the same order as the targets are returned by the targetList flag

        Returns:
            list
        """
        return self.mayaCommand(str(self), q=True, targetList=True)

    def weights(self):
        """
        Returns the channels that control the weight of the target objects.
        Weight channels are returned in the same order as the targets are returned by the targetList flag

        e.g. 'pointConstraint1.rootW0'

        Returns:
            list
        """
        weightAliases = self.mayaCommand(str(self), q=True, weightAliasList=True) or []
        return [NodChannel(self, w) for w in weightAliases] or []

    def setWeights(self, *args):
        """
        Set weight values

        Args:
            *args: Weight values

        Raises:
            ValueError: If the values count is not matching the weights count
        """
        weights = self.weights()
        if not len(args) == len(weights):
            raise ValueError('Incorrect amount of weight values specified ({}) for {} constraint weights'.format(args, len(weights)))

        for v, w in zip(args, weights):
            w.set(v)


class NodAimConstraint(NodConstraint, _OffsetMixin, _ConstraintTranslateMixin, _ConstraintVectorMixin):

    TYPE_NAME = 'aimConstraint'

    def __init__(self, *args):
        super(NodAimConstraint, self).__init__(*args)


class NodOrientConstraint(NodConstraint, _OffsetMixin, _ConstraintRotateMixin):

    TYPE_NAME = 'orientConstraint'

    def __init__(self, *args):
        super(NodOrientConstraint, self).__init__(*args)


class NodParentConstraint(NodConstraint, _ConstraintTranslateMixin, _ConstraintRotateMixin):

    TYPE_NAME = 'parentConstraint'

    def __init__(self, *args):
        super(NodParentConstraint, self).__init__(*args)


class NodPointConstraint(NodConstraint, _OffsetMixin, _ConstraintTranslateMixin):

    TYPE_NAME = 'pointConstraint'

    def __init__(self, *args):
        super(NodPointConstraint, self).__init__(*args)


class NodPoleVectorConstraint(NodConstraint, _OffsetMixin, _ConstraintTranslateMixin):

    TYPE_NAME = 'poleVectorConstraint'

    def __init__(self, *args):
        super(NodPoleVectorConstraint, self).__init__(*args)


class NodScaleConstraint(NodConstraint, _OffsetMixin, _ConstraintScaleMixin):

    TYPE_NAME = 'scaleConstraint'

    def __init__(self, *args):
        super(NodScaleConstraint, self).__init__(*args)
