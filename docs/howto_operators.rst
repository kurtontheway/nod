Operators :class:`nod.op`
--------------------------



There are two types of operators. **Node Operators** that take *nodes* as inputs, as well as **Channel operators** that operate on *channel* objects.


.. toctree::
   :maxdepth: 5

   howto_operators_channels
   howto_operators_nodes