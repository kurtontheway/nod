
Deleting nodes and attributes
------------------------------

There are two ways of deleting nodes

.. code-block:: python

	joint.delete()
	nc.delete(joint)

Attributes can be removed in the same fashion

.. code-block:: python

	control.stretch.delete()